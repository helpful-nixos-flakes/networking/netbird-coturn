# Netbird Coturn Server
Netbird uses Coturn for it's relay functionality.  This Nix flake will configure and run the necessary Coturn server for self-hosting Netbird.

The default `config.services.netbird.server.coturn` service was a bit problematic to configure.  The ACME certificates were clumsy and weren't implemented correctly as far as I could tell.  Some options were difficult to configure, even though they seemed like they should be configurable.  This flake was created as a workaround.  Much of the nixpkgs module is still present in this flake, however.

## Notes about implementation
* Ports are not configurable.  This wasn't a mistake, just didn't think it was necessary.  It can still be done by configuring the service directy with `config.services.coturn`.
* Ports are not open by default.  You can use the `openFirewall` option to automatically do so.
* TLS certs are required in this configuration.  You will need to configure ACME accordingly.