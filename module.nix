{ config, lib, pkgs, ... }:

let
  inherit (lib) getExe mkAfter mkEnableOption mkIf mkOption types;

  cfg = config.services.netbird-coturn;
  coturnCfg = config.services.coturn;
  coturnPorts = with coturnCfg; [
    listening-port
    alt-listening-port
    tls-listening-port
    alt-tls-listening-port
  ];
in {
  options.services.netbird-coturn = {
    enable = mkEnableOption "A Coturn server configured for self-hosting Netbird.";
    
    stunDomain = mkOption {
      type = types.str;
      default = cfg.turnDomain;
      example = "stun.mydomain.tld";
      description = "The domain for the STUN server.  Defaults to the TURN domain.";
    };

    turnDomain = mkOption {
      type = types.str;
      example = "turn.mydomain.tld";
      description = "The domain for the TURN server.  Required for certificates.";
    };

    listeningIPs = mkOption {
      type = types.listOf types.str;
      default = [ ];
      description = ''
        Listener IP addresses of relay server.  If none are specified,
        then all IPv4 addresses on the system will be used.
      '';
    };

    relayIPs = mkOption {
      type = types.listOf types.str;
      default = [ ];
      description = ''
        Listener IP addresses of relay server.  If none are specified,
        then all IPv4 addresses on the system will be used.
      '';
    };

    externalIP = mkOption {
      type = types.str;
      default = null;
      description = ''
        TURN Server public/private address mapping, if the server is behind NAT.
        This may be needed in virtual private cloud situations where there is a
        1:1 port-mapping between a public IP and the VPC IP.
      '';
    };

    user = mkOption {
      type = types.str;
      default = "netbird";
      description = "The username used by netbird to connect to the coturn server.";
    };

    password = mkOption {
      type = types.nullOr types.str;
      default = null;
      description = ''
        The password of the user used by netbird to connect to the coturn server.
        Note that this will be world-readable when used instead of passwordFile.
      '';
    };

    passwordFile = mkOption {
      type = types.nullOr types.path;
      default = null;
      example = /var/lib/netbird-coturn/password;
      description = "The path to a file containing the password of the user used by netbird to connect to the coturn server.";
    };

    openFirewall = mkOption {
      type = types.bool;
      default = false;
      description = ''
        Whether to automatically Coturn's ports in the firewall.
      '';
    };
  };

  config = mkIf cfg.enable {
    assertions = [
      {
        assertion = (cfg.password == null) != (cfg.passwordFile == null);
        message = "Exactly one of `password` or `passwordFile` must be given for the coturn setup.";
      }
    ];

    services.coturn = {
      enable = true;
      realm = cfg.turnDomain;
      listening-ips = cfg.listeningIPs;
      relay-ips = cfg.relayIPs;
      cert = "@cert@";
      pkey = "@pkey@";
      lt-cred-mech = true;
      no-cli = true;

      extraConfig = ''
        # recommended security
        no-rfc5780
        no-stun-backward-compatibility

        fingerprint
        user=${cfg.user}:${if cfg.password != null then cfg.password else "@password@"}
        no-software-attribute
        ${lib.optionalString (cfg.externalIP != null) "external-ip=${cfg.externalIP}"}
      '';
    };

    systemd.services.coturn = let
      certDir = config.security.acme.certs.${cfg.turnDomain}.directory;
    in {
      path = with pkgs; [ replace-secret ];
      preStart = mkAfter ''
        ${lib.optionalString
          (cfg.passwordFile != null)
          "replace-secret @password@ ${cfg.passwordFile} /run/coturn/turnserver.cfg"}
        sed -i "s|@cert@|$CREDENTIALS_DIRECTORY/cert.pem|" /run/coturn/turnserver.cfg
        sed -i "s|@pkey@|$CREDENTIALS_DIRECTORY/pkey.pem|" /run/coturn/turnserver.cfg
      '';

      # this loads the credentials into the sysetmd service
      serviceConfig.LoadCredential = [
        "cert.pem:${certDir}/fullchain.pem"
        "pkey.pem:${certDir}/key.pem"
      ];
    };

    security.acme.certs.${cfg.turnDomain} = {
      postRun = "systemctl restart coturn.service"; # pick up certs when created
      extraDomainNames = mkIf (cfg.stunDomain != cfg.turnDomain) [ cfg.stunDomain ];
    };

    networking.firewall = mkIf cfg.openFirewall {
      allowedTCPPorts = coturnPorts;
      allowedUDPPorts = coturnPorts;
      allowedUDPPortRanges = [ { from = coturnCfg.min-port; to = coturnCfg.max-port; } ];
    };
  };
}